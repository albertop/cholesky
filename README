Cholesky - Cholesky decomposition of a symmetric positive definite matrix

Copyright Information
    Copyright (C) 2012 Alberto Passalacqua

License
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Description
    Cholesky decomposition of a symmetric positive definite matrix

Target platform
    The code is known to work with OpenFOAM 2.1.x

Author
    Alberto Passalacqua <albert.passalacqua@gmail.com>
    Website: http://www.albertopassalacqua.com

Acknowledgments
    This implementation derives almost completely from TNT/JAMA, a public-domain
    library developed at NIST, available at http://math.nist.gov/tnt/index.html

Disclaimer
----------
This offering is not approved or endorsed by OpenCFD Limited, the producer
of the OpenFOAM software and owner of the OPENFOAM®  and OpenCFD®  trade marks.

Detailed information on the OpenFOAM trademark can be found at

 - http://www.openfoam.com/legal/trademark-policy.php
 - http://www.openfoam.com/legal/trademark-guidelines.php

For further information on OpenCFD and OpenFOAM, please refer to

 - http://www.openfoam.com